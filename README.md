# Lecture on version control with Git and Gitlab
This is a repository containing the source and presentation material for a small lecture/workshop organized by Marco Dalla Vecchia (dallavem@igbmc.fr) for users in a out IGBMC.

## Content
The lecture is mostly consisting of HTML slides available directly online through Gitlab pages.

- Source-markdown folder contains the original Markdown documents used to produce the HTML presentations. Slides were create in VScode via the [MARP](https://marp.app/) plugin.
- index.html contains the main slides of the lecture.
- gitlab-step-by-step.html contains a step-wise guide to setup a new Gitlab repository and a intro to basic usage of Gitlab through the web interface rich in screenshots.
- handson-workshop.html contains the backbone and guide for the associated hands-on workshop on version control and Gitlab.

## Check out the presentations
1. The main presentation is available here: [main presentation](https://igbmc.gitlab.io/mic-photon/version-control/version-control-gitlab/)
2. The step-by-step guide is available here: [step-by-step guide](https://igbmc.gitlab.io/mic-photon/version-control/version-control-gitlab/gitlab-step-by-step)
3. The associated hands-on workshop material is available here: [handson-workshop](https://igbmc.gitlab.io/mic-photon/version-control/version-control-gitlab/handson-workshop)