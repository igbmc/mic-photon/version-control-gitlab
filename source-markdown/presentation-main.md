---
marp: true
title: Version control lecture
paginate: true
author: Marco Dalla Vecchia
theme: default
header: '![right h:50](assets/images/centre-imagerie-logo.png) ![right h:50](assets/images/igbmc.png)'
footer: 'concept and illustrations from [version_control_tutorial](https://mvgroup.gitlab.io/version_control_tutorial/docs/presentation/)'
style: |
  .columns {
    display: grid;
    grid-template-columns: repeat(2, minmax(0, 1fr));
    gap: 1rem;
  }
---

<style>
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
</style>

# Version control with Git and Gitlab
Marco Dalla Vecchia

IGBMC mic-photon
![bg contain right](assets/images/git-logo.png)
![bg contain right](assets/images/gitlab-logo-png-transparent.png)

---


# Why version control?

## Has this ever happened to you?



---

![bg contain right](assets/illlustrations/problematic_workflow1.png)

## Why version control?

> You need to work on a project report and you take the lead to start the document

> Predicting collaboration you give it a version: _V1_

---

![bg contain right](assets/illlustrations/problematic_workflow2.png)

## Why version control?

> You send it to Bob and Ana to give you feedback and to work on their parts

---

![bg contain right](assets/illlustrations/problematic_workflow3.png)

## Why version control?

> In the meantime you continue working on your part!

---

![bg contain right](assets/illlustrations/problematic_workflow4.png)

## Why version control?

> Bob and Ana send you back their copies and now there are so many versions!

---

<div class="columns">
<div>

# Why version control?
Version control allows you to:

</div>
<div>

* Keep track of changes
* Integrate new features
* Structure and organize projects
* Share and collaborate with others

</div>
</div>

---
![bg contain right](assets/illlustrations/correct_workflow_fr_1.png)

## Here's how it works!

> We start with the original file. Each part (or **feature**) is colored differently.

---

![bg contain right](assets/illlustrations/correct_workflow_fr_2.png)

## Here's how it works!

> Each user gets access to its own version

> **Multiple copies are not created!**

---

![bg contain right](assets/illlustrations/correct_workflow_fr_3.png)

## Here's how it works!

> Each user **will work only on what previously agreed** by the team!

---

![bg contain right](assets/illlustrations/correct_workflow_fr_4.png)

## Here's how it works!

> When Ana is done, her work will update the original file **adding** her _feature_

---

![bg contain right](assets/illlustrations/correct_workflow_fr_5.png)

## Here's how it works!

> When Bob is done, his work will **modify** the _feature_ of the original file

---

![bg contain right](assets/illlustrations/correct_workflow_fr_5.png)

## We did it!

* We have achieved the final report in **one document** 
* While keeping the **workflow** and the **structure** well organized!

---

## Main advantages of version control

* Simply keep track of what you are doing through commits
* Go back to a previous version if you made a mistake
* Keep features compartimentalized
* Back up your work
* Easy to collaborate

---

<div class="columns">
<div>

# Git
![h:200 w:200 center](assets/images/git-logo.png)
It's **the version control tool** of choice
</div>
<div>

# Gitlab
![h:200 w:200 center](assets/images/gitlab-logo-png-transparent.png)
It's a **host and manager** of version-controlled repositories
</div>
</div>

---

![bg contain right](assets/images/git-logo.png)
# Git works locally

> Use it from the Git terminal/bash

## Software alternatives

- [Sublime Merge](https://www.sublimemerge.com/)
- [Github Desktop](https://desktop.github.com/)
  - [Github Desktop for Gitlab](https://itnext.io/how-to-use-github-desktop-with-gitlab-cd4d2de3d104)
- [Git Kraken](https://www.gitkraken.com/)
- And [many more](https://git-scm.com/download/gui/windows)

---

## Git and Gitlab key concepts

<div class="columns">

<div>

## Repository
- A folder that is version controlled
- Has a '.git' folder inside
- It can be either _local_ or _remote_
</div>

<div>

![repo-illustration](assets/illlustrations/repositories.png)
</div>

</div>

---

## Git and Gitlab key concepts

<div class="columns">

<div>

## Commits
- A '_checkpoint_' of your work
- Commit frequently and descriptively
- You **can move back and forth** between commits
</div>
<div>

![commit-illustration](assets/illlustrations/commits.png)
</div>
</div>

---

## Git and Gitlab key concepts

<div class="columns">

<div>

## Branches
- A separate '_line of work_'
- Allow to test new features
  - **Discard** easily if bad
  - **Merge** with main work if good
</div>
<div>

![branch-illustration](assets/illlustrations/branches.png)
</div>
</div>

---

![bg contain right](assets/images/gitlab-logo-png-transparent.png)
# Let's talk about Gitlab

---
## Gitlab interface: we have a group!

![h:450 center](assets/images/gitlab-group.png)

---
## Gitlab interface: create a new project
![h:450 center](assets/images/gitlab-new-project.png)

---
![bg contain right](assets/images/gitlab-logo-png-transparent.png)
# Gitlab demo

open [this link](https://gitlab.com/igbmc/mic-photon)

---
# Git & Gitlab typical workflow 
## Start a repository from scratch
```bash
$ git init
$ git branch -m main # set name of main branch
$ git remote add origin https://gitlab.com/igbmc/mic-photon/test-repo.git

# first commit
$ git add .
$ git commit -m "initial commit"
$ git push -u origin main
```

---
# Git & Gitlab typical workflow 
## Use an existing repository
```bash
# via https
$ git clone https://gitlab.com/igbmc/mic-photon/workshop-version-control.git

# via SSH
$ git clone git@gitlab.com:igbmc/mic-photon/workshop-version-control.git
```

---
# Git & Gitlab typical workflow 
## Implement changes and send it to Gitlab
```bash
$ git fetch / git pull # to retrieve up-to-date status of remote
$ git add .
$ git commit -m "commit message"
$ git push origin main
```

---
# Git & Gitlab typical workflow 
## Check history
```bash
$ git log
$ git log --oneline
```

---
# Git & Gitlab typical workflow 
## Check difference between things
```bash
$ git diff
$ git diff filename
$ git diff commit1 commit2
```

---
# Git & Gitlab typical workflow 
## Revert last commit
```bash
$ git revert HEAD
```

---
# OPTIONAL: setting up first steps
## Tell git who you are

```bash
$ git config --global user.name "First name Last name"
$ git config --global user.email “you@example.com”
```
---
# OPTIONAL: setting up first steps
## Create a SSH key to connect to Gitlab
Inside Git bash
```bash
$ ssh-keygen
```

---
# OPTIONAL: setting up first steps
## Create a SSH key to connect to Gitlab
Inside Git bash
```bash
$ ssh-keygen -t ed25519 -C "<comment>" # comment can be for example your email
# Give it a name
# Give it a password
```
This will create a hidden .ssh folder containing the public key

---
# OPTIONAL: setting up first steps
## Link SSH public key with Gitlab
Inside Git bash
```bash
$ cat .ssh/ed25519.pub
# select and copy output
```

---
# OPTIONAL: setting up first steps
Gitlab -> Edit Profile -> SSH Keys
![h:450 center](assets/images/gitlab-ssh-key.png)

---
# OPTIONAL: add key to current workflow
Inside Git bash
```bash
$ ssh-add # ~/.ssh/id_ed25519
# Insert password and identity will be added
```