---
marp: true
title: Version control guide
paginate: true
theme: default
style: |
  .columns {
    display: grid;
    grid-template-columns: repeat(2, minmax(0, 1fr));
    gap: 1rem;
  }
---

<style>
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
</style>

# Version control with Git and Gitlab
Marco Dalla Vecchia

IGBMC mic-photon
![bg contain right](assets/images/git-logo.png)
![bg contain right](assets/images/gitlab-logo-png-transparent.png)

---

# Work on Gitlab online
**Create a new project**
![h:450 center](assets/step-by-step/01-new-project.png)

---

# Work on Gitlab online
**Upload files**
![h:450 center](assets/step-by-step/02-project-created.png)

---

# Work on Gitlab online
**Upload files**
![h:450 center](assets/step-by-step/03-upload-file.png)

---

# Work on Gitlab online
**Check the commits**
![h:450 center](assets/step-by-step/04-check-commits.png)

---

# Work on Gitlab online
**Check changes on a file or commit**
![h:450 center](assets/step-by-step/05-check-commit-changes.png)

---

# Work on Gitlab online
**Change settings**
![h:450 center](assets/step-by-step/06-change-settings.png)

---

# Work on Gitlab online
**Update the readme file**
![h:450 center](assets/step-by-step/07-edit-readme.png)

---

# Work on Gitlab online
**Be clear and descriptive!**
![h:450 center](assets/step-by-step/08-readme-changed.png)

---

# Work on Gitlab online
**Look at the results of the last commit**
![h:450 center](assets/step-by-step/09-readm-commit-result.png)
