---
marp: true
title: Version control hands-on workshop slides
author: Marco Dalla Vecchia
paginate: true
theme: default
style: |
  .columns {
    display: grid;
    grid-template-columns: repeat(2, minmax(0, 1fr));
    gap: 1rem;
  }
---

<style>
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
</style>

# Version control with Git and Gitlab
Marco Dalla Vecchia

IGBMC mic-photon
![bg contain right](assets/images/git-logo.png)
![bg contain right](assets/images/gitlab-logo-png-transparent.png)

---

# Table of contents

1. Getting familiar with Gitlab
2. Creating your own repository on Gitlab
3. Setting up git
4. Establish connection with remote repository
5. Daily workflow
6. Branching
7. Collaborating
8. Solving merge conflicts
9. Reverse changes

---
## 1. Getting familiar with Gitlab
Check out [this workshop repository](https://gitlab.com/igbmc/mic-photon/workshop-version-control) as an example

![screenshot](assets/images/workshop-repo-screenshot.png)

---
## 2. Creating your own repository

<iframe id="iframe-slides"
    width="900"
    height="600"
    src="https://igbmc.gitlab.io/mic-photon/version-control-gitlab/gitlab-step-by-step#2">
</iframe>

[Gitlab step-by-step presentation](https://igbmc.gitlab.io/mic-photon/version-control-gitlab/gitlab-step-by-step#2)

---
## 3. Setting up git
Install [Git for Windows](https://gitforwindows.org/)
Leave most things as default --> [general instructions](https://phoenixnap.com/kb/how-to-install-git-windows)

### Windows explorare integration

![](https://phoenixnap.com/kb/wp-content/uploads/2021/04/git-installation-component-selection-screen.png)

---
## 3. Setting up git
Install [Git for Windows](https://gitforwindows.org/)

### Use your preferred text editor

![](https://phoenixnap.com/kb/wp-content/uploads/2021/04/select-text-editor-notepad-windows.png)

---
## 3. Setting up git

### Open Git bash from Windows Explorer

<iframe id="iframe-slides"
    width="900"
    height="600"
    src="https://igbmc.gitlab.io/mic-photon/version-control-gitlab/#30">
</iframe>

Version control main presentation [slide 30](https://igbmc.gitlab.io/mic-photon/version-control-gitlab/#30)

Add ssh key identity following [instructions from Gitlab](https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair)

---

## 3.5 Prepare mock local repository

- Create folder for repository and create a **good** structure.
Example of common structure:
    - Input (data + metadata + documentation)
    - Output (figures, tables, analysis etc..)
    - Exploration (anything that involves testing and playing around)
    - Analysis (scripts + documentation)

- Add a **README.md** file (markdown is the default for documenting work)
- Add the files you want to keep under version control to the structure

---

## 4. Establish connection with remote repository

### Connect a local repository with the Gitlab repo you created

<iframe id="iframe-slides"
    width="900"
    height="600"
    src="https://igbmc.gitlab.io/mic-photon/version-control-gitlab/#24">
</iframe>

Version control main presentation [slide 24](https://igbmc.gitlab.io/mic-photon/version-control-gitlab/#24)

---
## 4. Establish connection with remote repository
### Clone an existing repository

<iframe id="iframe-slides"
    width="900"
    height="600"
    src="https://igbmc.gitlab.io/mic-photon/version-control-gitlab/#25">
</iframe>

Version control main presentation [slide 25](https://igbmc.gitlab.io/mic-photon/version-control-gitlab/#25)

---
## 5. Daily workflow
<div class="columns">

<div>

<iframe id="iframe-slides"
    width="500"
    height="400"
    src="https://igbmc.gitlab.io/mic-photon/version-control-gitlab/#26">
</iframe>

Version control main presentation [slide 26](https://igbmc.gitlab.io/mic-photon/version-control-gitlab/#26)
</div>

<div class="mermaid">
graph TD;
    A(Work on files)--> B(Add files to keep track);
    B-->C(Commit checkpoints of work);
    C-->A;
    C-->D(Push to remote);
</div>
</div>

<!-- mermaid.js -->
<script src="https://unpkg.com/mermaid@8.1.0/dist/mermaid.min.js"></script>
<script>mermaid.initialize({startOnLoad:true});</script>

---

## 6. Branching

Integrate the following in the daily workflow
```bash
# git checkout StartOfBranch

$ git branch BranchName
$ git checkout BranchName

$ git status # to check where you are
$ git branch # to see local branches
$ git branch -a # to see all branches (local and remote)
```

### Keep _main_ branch stable, develop things in a _development_ branch

---
## 7. Collaborating

* **Communicate**
* **Create branches!**
    - For different developers
    - For different features
    - For temporary tests
* Pull and fetch **often**! &rarr; you want to work on the latest version
* Don't merge unless **everybody** agrees &rarr; use Gitlab merge requests
* Use [Issues](https://docs.gitlab.com/ee/user/project/issues/) to track tasks and discuss implementations

---
## 8. Solving merge conflicts

<div class="columns">
<div>

### Case 1: no conflicts! Yey!
```bash
$ git checkout main # to merge things into main
$ git merge BranchName

Updating 213cd72..9ccde4f
Fast-forward
 test.txt | 3 ++-
 1 file changed, 2 insertions(+), 1 deletion(-)

```
</div>

<div>

### Case 2: Merge conflicts :C
```bash
$ git checkout main # to merge things into main
$ git merge BranchName

Auto-merging test.txt
CONFLICT (content): Merge conflict in test.txt
Automatic merge failed; 
fix conflicts and then commit the result.

# Check the file with conflict!

this is a test2
<<<<<<< HEAD
I will change this whole line
=======
I also modified this line! oh no!
>>>>>>> branch1
```
</div>
</div>


---

## 9. Reverse changes

<div class="columns">
<div>

### Preferable: 
**undoes changes in new commit**
```bash
# It asks for a commit message
$ git revert HEAD

$ git log
commit 12dbf.. (branch1)
Author: marcodallavecchia <dallavem@igbmc.fr>
Date:   Wed Oct 5 10:39:39 2022 +0200

    The third line should not have been added
    
    This reverts commit 50cb9...

commit 50cb9..
Author: marcodallavecchia <dallavem@igbmc.fr>
Date:   Wed Oct 5 10:39:35 2022 +0200

    added a third line

```

</div>
<div>

### Dangerous: 
**modifies the history!**
```bash
# ^ symbol means previous commit
# Resets all files to previous commit

$ git reset --hard HEAD^

# In the log 
# we don't see the commit anymore
```
</div>
</div>